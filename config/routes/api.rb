namespace :api do
  namespace :v1 do
    #user
    post "/login", to: "users#login"
    post "/product_searchs/search", to: "product_searchs#search"
    get "/roles", to: "users#roles"
  end
end
