namespace :administration do
  resources :users, except: [:destroy]
  resources :areas

end
