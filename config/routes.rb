Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  #devise_for :users, skip: [:sessions]

  #as :user do
  #  get 'login', to: 'pages#login', as: :new_user_session
  #  get 'logout', to: 'devise/sessions#destroy', as: :destroy_user_session
  #end

  root to: "pages#index" 

  resources :products
  resources :categories

  #resources :billings, only: [:index, :show, :destroy]
  #workspace
  
  #namespace :workspace do
  #  resources :boards
  #end
  
  #resources :workspace
  


  def draw(routes_name)
    instance_eval(File.read(Rails.root.join("config/routes/#{routes_name}.rb")))
  end

  #draw :administration
  draw :api

end
