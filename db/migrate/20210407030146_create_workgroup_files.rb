class CreateWorkgroupFiles < ActiveRecord::Migration[6.0]
  def change
    create_table :workgroup_files, id: :uuid do |t|
      t.string :name
      t.text :description
      t.string :type
      t.references :user, type: :uuid, foreign_key: true
      t.json :data
      t.string :status

      t.timestamps
    end
  end
end
