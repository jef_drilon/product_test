class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products, id: :uuid do |t|
      t.string :refference_number
      t.string :name
      t.references :category, type: :uuid, foreign_key: true
      t.string :product_price
      t.string :string

      t.timestamps
    end
  end
end
