module Pages
  class GenerateRefference
    
    def initialize(config:)
      #raise config.inspect
      @loan_product  = Product.where(refference_number: config[:refference_number])
    
    end

    def execute!
      if @loan_product.count > 0
        @category_id = @loan_product.last.category_id
        @refference_number = @loan_product.last.refference_number
        @product_price = @loan_product.last.product_price
        
        query!
      
        @data = @result.map{ |o|
              
                
                tmp = {
                  ref_number: o.fetch("refference_number"),
                  refference_count: o.fetch("refference_count"),
                  category_count: o.fetch("category_count"),
                  product_price: o.fetch("product_price")
                }


                prod_data = Product.select("refference_number","name","category_id","product_price").where("refference_number = '#{tmp[:ref_number]}'")
                tmp_list = {}  
                tmp_list[:list] = []
                prod_data.each do |pd|
                  dates = {}
                  dates[:refference_number] = pd.refference_number
                  dates[:name] = pd.name
                  dates[:category] = pd.category.name

                  if tmp[:refference_count] >= 2 and tmp[:category_count] >= 2 and tmp[:product_price] >= 2
                    dates[:destination] = "DESTINATION 1"
                  else
                    dates[:destination] = "DESTINATION 2"
                  end
                  
                  tmp_list[:list] << dates
                  
                end

                tmp_list
                        
      
        }
      end

      @data
    end


  private
  
    def query!
       @result = ActiveRecord::Base.connection.execute(<<-EOS).to_a 
        SELECT
      	  	p1.refference_number as refference_number,
            count(p1.refference_number) as refference_count ,
            count(p1.category_id) as category_count,
            count(p1.product_price) as product_price

        FROM 
            products p1
      
        WHERE  
          p1.category_id = '#{@category_id}' or 
          p1.refference_number like '% #{@refference_number} %' and 
          p1.product_price = '#{@product_price}'
        group by p1.refference_number

      EOS
  end

  end
end
