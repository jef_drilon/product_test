require("@rails/ujs").start()
import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';

window.$ = window.jquery = jquery;

import "@fortawesome/fontawesome-free/js/all";

var jQuery = require("jquery");

// import jQuery from "jquery";
global.$ = global.jQuery = jQuery;
window.$ = window.jQuery = jQuery;

require("bootstrap");
require("admin-lte");

// "init" Objects
import Login from "../models/Login.js";
import Dashboard from "../models/Dashboard.js";


const hooks = {
  
  "pages/login":                  [Login],
  "pages/index":                  [Dashboard],

}

document.addEventListener("DOMContentLoaded", () => {
  const { route, payload } = JSON.parse($("meta[name='parameters']").attr('content')); 
  const authenticityToken   = $("meta[name='csrf-token']").attr('content');
  const options             = { authenticityToken, ...payload }

  console.log("payload:");
  console.log(payload);
  console.log("route: " + route);

  const components = hooks[route];

  if (components) {
    components.forEach((component) => {
      if (typeof component.init === "function") { 
        component.init(options)
      } else {
        renderComponent(component, options)
      }

    })

  }

});

