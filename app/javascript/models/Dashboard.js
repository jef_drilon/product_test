import Mustache from "mustache/mustache";


var url         = "/api/v1//product_searchs/search";
var loadingText = '<i class="fa fa-spin"></i> Loading...';
var errorList   = "";
var isLoading   = false;

var $inputSearch;
var $inputPassword;
var $btnSearch;
var $message;

var authenticityToken;

var _cacheDom = function() {
  errorList         = $("#template-error-list").html();
  authenticityToken = $("meta[name='csrf-token']").attr('content');

  $inputSearch      = $("#input-search");
  $inputPassword      = $("#input-password");
  $btnSearch           = $("#btn-search");
  $message            = $(".message");
};

var _bindEvents = function() {
  $inputSearch.focus();

  $inputSearch.keyup(function(e) {
    if(e.keyCode == 13) {
      $btnLogin.click();
    }
  });


  $btnSearch.on("click", function() {
 
    var data  = {
      prod_ref: $inputSearch.val(),
      authenticity_token: authenticityToken
    };

    _toggleInput();

    $.ajax({
      url: url,
      method: 'POST',
      data: data,
      dataType: 'json',
      success: function(response) {
        window.location.href = "/products/" + encodeQueryData(response) ;
      },
      error: function(response) {
        try {
          var payload = JSON.parse(response.responseText);
          var errors  = payload.errors.full_messages;
          $message.html(
            Mustache.render(
              errorList,
              { errors: errors }
            )
          );

          _toggleInput();

          $inputSearch.focus();
        } catch(e) {
          console.log(e);
          $message.html("Invalid Refference number...");
          _toggleInput();
        }
      }
    });
  });
};

var _toggleInput = function() {
  isLoading = !isLoading;
  $inputPassword.prop("disabled", isLoading);
  

  if(isLoading) {
    $btnSearch.data('original-text', $btnSearch.html());
    $btnSearch.html(loadingText);
  } else {
    $btnSearch.html($btnSearch.data('original-text'));
  }
};

var init  = function() {
  _cacheDom();
  _bindEvents();
};

export default { init: init };
