class User < ApplicationRecord
  ROLES = [
    "ADMIN",
    "USER"
  ]


  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable, authentication_keys: [:login]


  attr_accessor :login

  validates :role, presence: true, inclusion: { in: ROLES}
  

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      if self.new_record?
        self.role == "USER" if self.role.nil?
      end

    end
  end

  def admin?
    role == "ADMIN"
  end



  def login=(login)
    @login  = login
  end

  def login
    @login || self.username || self.email
  end





  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { value: login.downcase }]).first 
    else
      if conditions[:username].nil?
        where(conditions).first
      else
        where(username: conditions[:username]).first
      end
    end
  end


end
