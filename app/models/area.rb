class Area < ApplicationRecord
  
  def load_defaults
    if self.new_record?
      self.status = "active"
    end
  end

end
